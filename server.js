import mongoose from 'mongoose';
import app from './app';

const DB = process.env.DATABASE_LOCAL;

mongoose
  .connect(DB, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindandModify: false
  })
  .then(() => {
    console.log('DB connection successful!');
  })
  .catch(err => console.log(err));


const port = process.env.PORT || 3000;

app.listen(port, () => {
  console.log(`App running on port ${port}...`);
});
