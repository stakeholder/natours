export {
  getAllUsers,
  createUser,
  getUser,
  patchUser,
  deleteUser
} from './userController';

export {
  getAllTours,
  createTour,
  getTour,
  patchTour,
  deleteTour
} from './tourController';