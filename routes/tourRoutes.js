import { Router } from 'express';

import {
  getAllTours,
  createTour,
  getTour,
  patchTour,
  deleteTour
} from '../controllers';

const router = Router();

router
  .route('/')
  .get(getAllTours)
  .post(createTour);

router
  .route('/:id')
  .get(getTour)
  .patch(patchTour)
  .delete(deleteTour);

export default router;
