import { Router } from 'express';
import {
  getAllUsers,
  createUser,
  getUser,
  patchUser,
  deleteUser
} from '../controllers';

const router = Router();

router
  .route('/')
  .get(getAllUsers)
  .post(createUser);

router
  .route('/:id')
  .get(getUser)
  .patch(patchUser)
  .delete(deleteUser);

export default router;
