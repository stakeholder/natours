import express, { json } from 'express';
import morgan from 'morgan';
import dotenv from 'dotenv';

import { tourRouter, userRouter } from './routes';

const app = express();
dotenv.config();

// Middlewares
app.use(json());

if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
}

// Routes
app.use('/api/v1/tours', tourRouter);
app.use('api/v1/users', userRouter);

export default app;
